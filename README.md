# xkcd

Golang tool to download and index xkcd comics `text` from [xkcd](https://xkcd.com/).

A solution for [**Exercise 4.12**](https://books.google.com.eg/books?redir_esc=y&id=SJHvCgAAQBAJ&q=Exercise+4.12#v=snippet&q=Exercise%204.13&f=false) in [**The Go Programming Language**](https://www.amazon.com/Programming-Language-Addison-Wesley-Professional-Computing/dp/0134190440).

## Installation

```bash
$ go get bitbucket.org/mohan3d/xkcd
$ cd $GOPATH/src/bitbucket.org/mohan3d/xkcd
$ go install
```

## Usage

```bash
$ xkcd -term <TERM_TO_BE_SEARCHED>
```
