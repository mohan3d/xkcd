package index

import (
	"encoding/gob"
	"os"
	"strings"
)

// XkcdEntry describes data to be saved in Xkcd Index.
type XkcdEntry struct {
	Transcript string
	ID         int
}

// XkcdIndex describes data storage/index for Xkcd comics.
type XkcdIndex map[string][]*XkcdEntry

// Add adds tokens of key into XkcdIndex.
func (index XkcdIndex) Add(key string, entry *XkcdEntry) {
	index.AddTrimmed(key, entry, func(s string) string {
		return s
	})
}

// AddTrimmed adds tokens of key into XkcdIndex
// Apply trim for each token before it's added.
func (index XkcdIndex) AddTrimmed(key string, entry *XkcdEntry, trim func(string) string) {
	for _, k := range strings.Split(key, " ") {
		trimmedKey := trim(k)
		index[trimmedKey] = append(index[trimmedKey], entry)
	}
}

// Search searchs XkcdIndex for key/word, returns slice of
// XkcdEntry and a boolean indicating existance of the key.
func (index XkcdIndex) Search(key string) ([]*XkcdEntry, bool) {
	v, ok := index[key]
	return v, ok
}

// New creates new XkcdIndex.
func New() XkcdIndex {
	var index XkcdIndex = make(map[string][]*XkcdEntry)
	return index
}

// Save dumps XkcdIndex to a file so it can be used later.
func Save(filePath string, index XkcdIndex) error {
	file, err := os.Create(filePath)
	if err != nil {
		return err
	}

	err = gob.NewEncoder(file).Encode(index)
	if err != nil {
		return err
	}

	return nil
}

// Load read a file and returns saved XkcdIndex.
func Load(filePath string) (XkcdIndex, error) {
	file, err := os.OpenFile(filePath, os.O_RDONLY, 0644)
	if err != nil {
		return nil, err
	}

	var index XkcdIndex
	err = gob.NewDecoder(file).Decode(&index)
	if err != nil {
		return nil, err
	}

	return index, nil
}
