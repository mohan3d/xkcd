package index

import (
	"os"
	"path/filepath"
	"reflect"
	"testing"
)

const filename = "xi.index"

var entries = []*XkcdEntry{
	{ID: 1, Transcript: "Transcript1"},
	{ID: 2, Transcript: "Transcript2"},
	{ID: 3, Transcript: "Transcript3"},
	{ID: 4, Transcript: "Transcript4"},
}

func addElements(index XkcdIndex, elements []*XkcdEntry) {
	for _, entry := range entries {
		index.Add(string(entry.ID), entry)
	}
}

func TestAddElements(t *testing.T) {
	index := New()
	addElements(index, entries)

	if len(index) != len(entries) {
		t.Errorf("expected %d elements got %d", len(entries), len(index))
	}
}

func TestSearchElements(t *testing.T) {
	index := New()
	addElements(index, entries)

	for _, entry := range entries {
		v, ok := index.Search(string(entry.ID))
		if !ok {
			t.Errorf("key %d not found", entry.ID)
		}
		if v[0] != entry {
			t.Errorf("expected %v got %v", entry, v[0])
		}
	}
}

func TestSaveIndex(t *testing.T) {
	index := New()
	addElements(index, entries)

	filePath := filepath.Join(os.TempDir(), filename)
	err := Save(filePath, index)

	if err != nil {
		t.Errorf("expected no errors got %s", err)
	}
}

func TestLoadIndex(t *testing.T) {
	var index, loadedIndex XkcdIndex
	index = New()
	addElements(index, entries)

	filePath := filepath.Join(os.TempDir(), filename)
	err := Save(filePath, index)

	if err != nil {
		t.Fatal(err)
	}

	loadedIndex, err = Load(filePath)

	if err != nil {
		t.Fatal(err)
	}

	if eq := reflect.DeepEqual(index, loadedIndex); !eq {
		t.Errorf("expected %v got %v", index, loadedIndex)
	}
}
