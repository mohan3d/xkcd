package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/mohan3d/xkcd/index"
)

const (
	cachePath    = "cache"
	comicBaseURL = "https://xkcd.com/%d/info.0.json"
	comicsCount  = 2000
)

type xkcdComic struct {
	Month      string `json:"month"`
	Num        int    `json:"num"`
	Link       string `json:"link"`
	Year       string `json:"year"`
	News       string `json:"news"`
	SafeTitle  string `json:"safe_title"`
	Transcript string `json:"transcript"`
	Alt        string `json:"alt"`
	Img        string `json:"img"`
	Title      string `json:"title"`
	Day        string `json:"day"`
}

func getComic(comicURL string) (*xkcdComic, error) {
	resp, err := http.Get(comicURL)

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var comic xkcdComic
	err = json.NewDecoder(resp.Body).Decode(&comic)

	if err != nil {
		return nil, err
	}
	return &comic, nil
}

func getComics() []*xkcdComic {
	var comics []*xkcdComic

	for i := 1; i <= comicsCount; i++ {
		url := fmt.Sprintf(comicBaseURL, i)
		comic, err := getComic(url)

		if err != nil {
			continue
		}
		comics = append(comics, comic)
	}
	return comics
}

func createIndex(comics []*xkcdComic) index.XkcdIndex {
	xi := index.New()

	for _, comic := range comics {
		entry := &index.XkcdEntry{
			Transcript: comic.Transcript,
			ID:         comic.Num,
		}
		xi.Add(comic.Transcript, entry)
	}
	return xi
}

func saveIndex(xi index.XkcdIndex) {
	err := index.Save(cachePath, xi)
	if err != nil {
		panic(err)
	}
}

func loadIndex() index.XkcdIndex {
	xi, err := index.Load(cachePath)
	if err != nil {
		panic(err)
	}

	return xi
}

func uniqueEntries(entries []*index.XkcdEntry) []*index.XkcdEntry {
	var lst []*index.XkcdEntry
	m := make(map[int]*index.XkcdEntry)

	for _, entry := range entries {
		m[entry.ID] = entry
	}

	for _, entry := range m {
		lst = append(lst, entry)
	}
	return lst
}

func main() {
	term := flag.String("term", "", "term to be searched")
	nocache := flag.Bool("nocache", false, "don't use cached index")
	sync := flag.Bool("sync", false, "re-download comics and save them")
	flag.Parse()

	var xi index.XkcdIndex
	if *sync || *nocache {
		fmt.Println("Downloading comics...")
		comics := getComics()
		fmt.Println("Creating index...")
		xi = createIndex(comics)

		if *sync {
			fmt.Println("Saving ...")
			saveIndex(xi)
		}
	}

	if len(*term) > 0 {
		if !(*nocache && *sync) {
			fmt.Println("Loading index...")
			xi = loadIndex()
		}

		entries, ok := xi.Search(*term)
		entries = uniqueEntries(entries)

		if !ok {
			fmt.Println("Nothing found!")
		} else {
			fmt.Printf("Found %d entry\n", len(entries))
			separator := strings.Repeat("-", 80)
			for _, entry := range entries {
				fmt.Printf("%s\n%s\n%s\n",
					separator,
					fmt.Sprintf(comicBaseURL, entry.ID),
					entry.Transcript,
				)
			}
		}
	}
}
